import imp
from folktables import ACSDataSource, ACSIncome
import json
import pandas as pd
from pathlib import Path

def make_income():

    # Create domain
    domain_path = Path("domain_income.json")
    if not domain_path.exists():
        domain = {
          "AGEP": {"type": "ORD", "range": 100},
          "COW": {"type": "CAT", "range": 9},
          "SCHL": {"type": "CAT", "range": 24},
          "MAR": {"type": "CAT", "range": 5},
          "RELP": {"type": "CAT", "range": 18},
          "WKHP": {"type": "ORD", "range": 99},
          "SEX": {"type": "CAT", "range": 2},
          "RAC1P": {"type": "CAT", "range": 9},
          "LABEL": {"type": "CAT", "range": 2}
        }
        json.dump(domain, open(domain_path, "w+"))

    df_path = Path("income.csv")
    if not df_path.exists():
        data_source = ACSDataSource(survey_year='2018', horizon='1-Year', survey='person')
        acs_data = data_source.get_data(states=["CA"], download=True)
        features, labels, _ = ACSIncome.df_to_numpy(acs_data)

        # Construct data
        df = pd.DataFrame(features, columns=ACSIncome.features, index=None)
        df.insert(0, "LABEL", labels)

        # Subsample for efficency
        df = df.sample(frac=0.01).drop_duplicates()

        df.to_csv("income.csv", index=False)

if __name__ == "__main__":
    make_income()
