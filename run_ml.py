import json
import numpy as np
import pandas as pd
from pathlib import Path
from tqdm.auto import tqdm

from synthetic.mechanisms import Domain

# Classifiers
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier

# Pipeline
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.pipeline import make_pipeline

# Metric
from sklearn.metrics import f1_score



def test(clf, label, df_train, df_test, domain):

    # Get labels
    y_train = df_train[label]
    y_test =  df_test[label]
    
    # Get train data
    enc = OneHotEncoder(sparse=False, handle_unknown="ignore")
    cat_idx = [l for l in domain.get_categorical_columns() if l != label]
    cat_train = enc.fit_transform(df_train[cat_idx])
    cat_test = enc.transform(df_test[cat_idx])

    num_idx = domain.get_continuous_columns() + domain.get_ordinal_columns()
    num_train = df_train[num_idx].values
    num_test = df_test[num_idx].values

    X_train = np.concatenate([cat_train, num_train], axis=1)
    X_test = np.concatenate([cat_test, num_test], axis=1)

    # Train and predict
    clf.fit(X_train, y_train)
    return f1_score(y_test, clf.predict(X_test), average='macro')


def add_experiment(run_dir, results):
    scores = {name: {label: [] for label in domain.get_categorical_columns()} for name in classifier_map}

    for file in run_dir.glob("*.csv"):

        # Load synthetic data
        df_synth = pd.read_csv(file)

        # Test different classifiers for different categoricalfeatures
        for name, clf in classifier_map.items():
            for label in domain.get_categorical_columns():
                  try:
                    scores[name][label].append(test(clf, label, df_synth, df_base, domain))
                  except:
                      pass

        # Save results to results
        name = run_dir.name
        mechanism, eps = name.split("_")

        # Handle edge case
        if mechanism not in results:
            results[mechanism] = {}

        results[mechanism][eps] = scores


if __name__ == "__main__":

    data_dir = Path("data_folktables")
    out = Path("classifiers_results_folktables.json")
    
    df_base = pd.read_csv("income.csv")
    domain = Domain.load("domain_income.json")

    classifier_map = {
        "SVC": make_pipeline(StandardScaler(), SVC(gamma='auto')),
        "Logistic Regression": make_pipeline(StandardScaler(), LogisticRegression()),
        "Gradient Boosting": make_pipeline(StandardScaler(), GradientBoostingClassifier()),
        "Ada Boosting": make_pipeline(StandardScaler(), AdaBoostClassifier()),
        "MLP": MLPClassifier(alpha=0.001, max_iter=1000),
    }

    results = {}
    for run_dir in tqdm(list(data_dir.glob("*/"))):
        add_experiment(run_dir, results)

    json.dump(results, open(out, "w+"))
