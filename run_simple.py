from itertools import product
from pathlib import Path
from tqdm.auto import tqdm

from synthetic.mechanisms import DPCTGANWrapper, PATECTGANWrapper, DPGANWrapper, PATEGANWrapper, MSTWrapper, MWEM_PGMWrapper, RONWrapper
from synthetic.experiments import Experiment

data_map = {
  "test": Path("test.csv"),
  "income": Path("income.csv")
}
domain_map = {
  "test": Path("domain.json"),
  "income": Path("domain_income.json")
}

label = "test"
dataset_path, domain_path = data_map[label], domain_map[label]
n_runs = 1
eps_values = [1.0]
mechansims = [MSTWrapper]

for eps, mechanism in tqdm(list(product(eps_values, mechansims))):
    experiment = Experiment.get_experiment(eps, n_runs, mechanism, dataset_path, domain_path)
    experiment.run()
