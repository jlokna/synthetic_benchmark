from copy import copy
from matplotlib import cm
import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import numpy as np

from synthetic.mechanisms import Domain

def get_synth(marginals, df_synth):
    marginals_synth = {}

    for col in marginals:
        marginals_synth[col] = {
            value: np.count_nonzero(df_synth[col] == value) / df_synth[col].size 
            for value in marginals[col]
        }

    return marginals_synth

def get_ref(df_base, domain):
    marginals = {}

    for col in domain.get_categorical_columns() + domain.get_ordinal_columns():
        u, c = np.unique(df_base[col], return_counts=True)
        marginals[col] = {value: count / df_base[col].size for value, count in zip(u, c)}

    return marginals

def score(marginals, marginals_synth):

    score = np.mean([
        np.linalg.norm(
            np.array(list(marginals[col].values())) - \
            np.array(list(marginals_synth[col].values()))
        )
    for col in marginals])

    return score

def add_experiment(run_dir, marginals, results):

    scores = []

    for file in run_dir.glob("*.csv"):
        df_synth = pd.read_csv(file)
        marginals_synth = get_synth(marginals, df_synth)
        scores.append(score(marginals, marginals_synth))
    
    name = run_dir.name
    mechanism, eps = name.split("_")

    if mechanism not in results:
        results[mechanism] = {}

    results[mechanism][eps] = {}
    results[mechanism][eps]["mean"] = np.mean(scores)
    results[mechanism][eps]["std"] = np.std(scores)


def get_colors(data):
    colors = copy(data.values)
    for i, row in enumerate(colors):
        for j, value in enumerate(row):
            if "\u00B1" in value:
                colors[i, j] = cm.get_cmap("seismic")(float(value.split("\u00B1")[0]) / 5 + 0.5)
            else:
                colors[i, j] = "w"
    return colors

def make_plots(results):

    data = pd.DataFrame.from_dict({mechanism: {eps: f"{value['mean'] :.2f} \u00B1 {value['std'] :.2f}" for eps, value in sub_res.items()} for mechanism, sub_res in results.items()})
    t = plt.table(cellText=data.values, colLabels=data.columns, rowLabels=list(data.index), loc="center", cellColours=get_colors(data))
    t.scale(1, 3)
    plt.title("Marginals")
    plt.axis("off")
    plt.savefig("./plots/results.png", dpi=300)

data_dir = Path("data_finished")
df_base = pd.read_csv("test.csv")
domain = Domain.load("domain.json")

marginals = get_ref(df_base, domain)

results = {}
for run_dir in data_dir.glob("*/"):
    add_experiment(run_dir, marginals, results)

make_plots(results)
