from copy import deepcopy
import json
from multiprocessing import Pool
import pandas as pd
from pathlib import Path

from ..mechanisms import Domain, OpenDPWrapper, MSTWrapper, MWEM_PGMWrapper, RONWrapper

class Experiment:

    def __init__(self, epsilon, params, run_params, n_runs, method, data_path, domain_path, out_dir, n_parralel):
        
        # Which levels for epsilon to use
        self.epsilon = epsilon
        
        # Parameters used during intialization and calling
        self.params = params
        self.run_params = run_params

        # Number of runs for each epsilon level
        self.n_runs = n_runs

        # Method to use
        self.method = method

        # Folder structure
        self.data_path = data_path
        self.domain_path = domain_path
        self.out_dir = out_dir

        # Paralell processing
        self.n_parralel = n_parralel

        # Create folder and make sure it does not already exist
        self.curr_dir = self.out_dir.joinpath(f"{self.method.name()}_{self.epsilon}")
        self.curr_dir.mkdir(exist_ok=False, parents=True)

        # Helper variables
        self.df = None
        self.domain = None

    def run(self):

        # Load data
        self.domain = Domain.load(self.domain_path)
        
        self.df = pd.read_csv(self.data_path)[
            self.domain.get_continuous_columns() 
            + self.domain.get_categorical_columns() 
            + self.domain.get_ordinal_columns()
        ]

        # Save parameters
        p = deepcopy(self.params)
        p.update(self.run_params)
        p.update({"epsilon": self.epsilon})
        json.dump(p, open(self.curr_dir.joinpath("params.json"), "w+"))

        # Run in parallel
        Pool(self.n_parralel).map(self.single_run, range(self.n_runs))

        # Finish up and close files
        self.df = None
        self.domain = None

    def single_run(self, idx):
    
            # Create synthetic dataset
            synthetic_df = self.method(epsilon = self.epsilon, **self.params).make_synthetic(
                df=self.df, domain=self.domain, **self.run_params
            )

            # Save the new dataset
            path = self.curr_dir.joinpath(f"dataset_{idx}.csv")
            synthetic_df.to_csv(path)

    @staticmethod
    def get_experiment(epsilon, n_runs, method, data_path, domain_path):

        # Some default parameters used by all experiments
        out_dir = Path("./data_synth")

        if issubclass(method, OpenDPWrapper):
            params = {}
            run_params = {"preprocessor_eps": 0.2}

        elif issubclass(method, MSTWrapper):
            params = {"delta": 1e-6}
            run_params = {}

        elif issubclass(method, MWEM_PGMWrapper):
            params = {"delta": 1e-6}
            run_params = {}

        elif issubclass(method, RONWrapper):
            params = {"delta": 1e-6}
            run_params = {}
        
        else:
            raise NotImplementedError()

        return Experiment(epsilon, params, run_params, n_runs, method, data_path, domain_path, out_dir, n_runs)
