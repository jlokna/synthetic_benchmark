import json
from pathlib import Path

class Domain:

    categorical_token = "CAT"
    continuous_token = "CON"
    ordinal_token = "ORD"

    def __init__(self, data = {}) -> None:
        self.data = data

    def get_columns(self):
        return list(self.data.keys)

    def get_categorical_columns(self):
        return [key for key, value in self.data.items() if value["type"] == self.categorical_token]

    def get_ordinal_columns(self):
        return [key for key, value in self.data.items() if value["type"] == self.ordinal_token]

    def get_continuous_columns(self):
        return [key for key, value in self.data.items() if value["type"] == self.continuous_token]
    
    def get_shape(self):
        return {key: value["range"] for key, value in self.data.items()}

    @staticmethod
    def load(path : Path):
        with open(path, "r") as input_file:
            data = json.load(input_file)
            return Domain(data)
