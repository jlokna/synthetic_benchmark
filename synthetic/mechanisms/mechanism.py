from abc import ABC, abstractmethod, abstractproperty
import pandas as pd
from pathlib import Path

from . import Domain

class SyntheticDataMechanism(ABC):

      def __init__(self, **params) -> None:
          super().__init__()
          self.build_dir.mkdir(exist_ok=True, parents=True)
          self.params = params

          # Run code to accuire the  mechanism
          self.get_source()

      @property
      def build_dir(self):
          return Path(__file__).parent.joinpath("build_dir")

      @staticmethod   
      @abstractmethod
      def name():
          pass

      @abstractmethod
      def get_source(self):
          pass
      
      @abstractmethod
      def make_synthetic(self, df : pd.DataFrame, domain : Domain, **kwargs):
          pass
