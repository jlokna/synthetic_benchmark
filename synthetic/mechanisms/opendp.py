from abc import abstractproperty
import subprocess
import pandas as pd
import sys

from . import SyntheticDataMechanism, Domain

class OpenDPWrapper(SyntheticDataMechanism):

      @property
      def git_dir(self):
          return self.build_dir.joinpath("smartnoise-sdk")

      @abstractproperty
      def synthesizer(self):
          pass

      def get_source(self):

          try:
              import snsynth
          
          except Exception as e:

              if not self.git_dir.exists():
                  cmd_clone = f"cd {self.build_dir} ; git clone https://github.com/opendp/smartnoise-sdk.git"
                  subprocess.run(["/bin/bash", "-c", cmd_clone], stdout=subprocess.PIPE)

              # Create file which causes problems if not present
              problemtic_file = self.git_dir.joinpath("synth/snsynth/pytorch/nn/ctgan/__init__.py")
              if not problemtic_file.exists():
                  cmd_create_problematic = f"touch {problemtic_file}"
                  subprocess.run(["/bin/bash", "-c", cmd_create_problematic], stdout=subprocess.PIPE)

              # Install file - instead of adding to path
              cmd_install = f"cd {self.git_dir.joinpath('synth')} ; python3 setup.py install --user"
              subprocess.run(["/bin/bash", "-c", cmd_install], stdout=subprocess.PIPE)

      def make_synthetic(self, df : pd.DataFrame, domain : Domain, preprocessor_eps : float):

          # Test validity
          if preprocessor_eps > 1 or preprocessor_eps < 0:
              raise RuntimeError(f"The preprocessor epsilon ({preprocessor_eps}) must be in the range [0, 1]")

          # Train model
          self.model = self.synthesizer(**self.params)
          self.model.train(
              df,
              preprocessor_eps=preprocessor_eps * self.params["epsilon"],
              categorical_columns=domain.get_categorical_columns(),
              ordinal_columns=domain.get_ordinal_columns(),
              continuous_columns=domain.get_continuous_columns()
          )

          # Return a Data Frame with same size as the original data
          return self.model.generate(len(df))


class DPCTGANWrapper(OpenDPWrapper):

    @property
    def synthesizer(self):
        from snsynth.pytorch.nn import DPCTGAN
        return DPCTGAN

    @staticmethod
    def name():
        return "DPCTGAN"


class PATECTGANWrapper(OpenDPWrapper):

    @property
    def synthesizer(self):
        from snsynth.pytorch.nn import PATECTGAN
        return PATECTGAN

    @staticmethod
    def name():
        return "PATECTGAN"


class DPGANWrapper(OpenDPWrapper):

    @property
    def synthesizer(self):
        from snsynth.pytorch.nn import DPGAN
        return DPGAN

    @staticmethod
    def name():
        return "DPGAN"


class PATEGANWrapper(OpenDPWrapper):

    @property
    def synthesizer(self):
        from snsynth.pytorch.nn import PATEGAN
        return PATEGAN

    @staticmethod
    def name():
        return "PATEGAN"
