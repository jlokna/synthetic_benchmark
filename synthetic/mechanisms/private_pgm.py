from abc import abstractproperty
from copy import deepcopy
import numpy as np
import pandas as pd
import subprocess
import sys

from . import SyntheticDataMechanism, Domain


class PrivatePGMWrapper(SyntheticDataMechanism):

      @property
      def git_dir(self):
          return self.build_dir.joinpath("private-pgm")

      @abstractproperty
      def synthesizer(self):
          pass

      def get_source(self):

          if not self.git_dir.exists():
              cmd = f"cd {self.build_dir} ; git clone https://github.com/ryan112358/private-pgm.git"
              subprocess.run(["/bin/bash", "-c", cmd], stdout=subprocess.PIPE)
          
          # Add relevant directories to path
          mechanisms_path = self.git_dir.joinpath("mechanisms")
          if str(mechanisms_path) not in sys.path:
              sys.path.append(str(mechanisms_path))
          
          # Clean up files and make them Python compatible
          for file in mechanisms_path.glob("**/*+*"):
              file.rename(mechanisms_path.joinpath(str(file.name).replace("+", "_")))

          # Add to path
          src_path = self.git_dir.joinpath("src")
          if str(src_path) not in sys.path:
              sys.path.append(str(src_path))

      def make_synthetic(self, df : pd.DataFrame, domain : Domain):

            # Discretize the dataframe
            df = deepcopy(df)
            self.distretize_columns(df, domain)

            # Create the domain and the dataset
            from mbi import Dataset
            from mbi import Domain as MBIDomain

            shape = domain.get_shape()
            mbi_domain = MBIDomain(list(shape.keys()), list(shape.values()))
            dataset = Dataset(df, mbi_domain)

            # Fit the dataset and return the synthetic data
            return self.synthesizer(dataset, **self.params).df

      def distretize_columns(self, df : pd.DataFrame, domain : Domain):

          for col in domain.get_continuous_columns():
              lower = domain.data[col]["min"]
              upper = domain.data[col]["max"]
              shape = domain.data[col]["range"]
              df.assign(**{col: ((df[col].values - lower) * shape / (upper - lower)).astype(np.int64)})


class MSTWrapper(PrivatePGMWrapper):

    @property
    def synthesizer(self):
        from mst import MST
        return MST
          
    @staticmethod
    def name():
        return "MST"


class MWEM_PGMWrapper(PrivatePGMWrapper):

    @property
    def synthesizer(self):
        from mwem_pgm import mwem_pgm
        return mwem_pgm
          
    @staticmethod
    def name():
        return "MWEM-PGM"
