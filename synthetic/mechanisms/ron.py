from abc import abstractproperty
import subprocess
import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder
import sys

from . import SyntheticDataMechanism, Domain

class RONWrapper(SyntheticDataMechanism):

      def __init__(self, **params) -> None:
          super().__init__(**params)
          self.enc = None

      @property
      def git_dir(self):
          return self.build_dir.joinpath("private-data-generation")

      @staticmethod
      def name():
          return "RON"

      def get_source(self):

          if not self.git_dir.exists():
              cmd_clone = f"cd {self.build_dir} ; git clone https://github.com/BorealisAI/private-data-generation.git"
              subprocess.run(["/bin/bash", "-c", cmd_clone], stdout=subprocess.PIPE)

          # Add to path
          src_path = self.git_dir.joinpath("models")
          if str(src_path) not in sys.path:
              sys.path.append(str(src_path))

      def make_synthetic(self, df : pd.DataFrame, domain : Domain):

          # Pre-process data
          values = self._pre_process(df, domain)

          # Model parameters
          conditional = True
          n_sampples = values.shape[0]
          input_dim = values.shape[1]
          z_dim = int(input_dim / 4 + 1) if input_dim % 4 == 0 else int(input_dim / 4)

          from ron_gauss import RONGauss
          model = RONGauss(z_dim=z_dim, target_epsilon=self.params["epsilon"], target_delta=self.params["delta"], conditional=conditional)
          synth, _, mu = model.generate(values, y=np.zeros(n_sampples), n_samples=n_sampples)
          synthetic_values = mu[0] + synth

          # Postprocess into same shape
          df_synth = self._post_process(synthetic_values, domain)

          return df_synth

      def _pre_process(self, df : pd.DataFrame, domain : Domain):

          # Handle categorical features
          self.enc = OneHotEncoder(sparse=False)
          cat_values = self.enc.fit_transform(df[domain.get_categorical_columns()])
          self.n_cats = cat_values.shape[1]

          # Handle ordinal features
          ord_values = df[domain.get_ordinal_columns()].values.astype(np.float64)
          self.n_ords = ord_values.shape[1]

          # Handle continous features
          con_values = df[domain.get_continuous_columns()].values.astype(np.float64)
          self.n_cons = con_values.shape[1]

          return np.concatenate([cat_values, ord_values, con_values], axis=1)
      
      def _post_process(self, values : np.ndarray, domain : Domain):

          # Reconstruct categorical features
          cat_synth = pd.DataFrame(
              self.enc.inverse_transform(values[:, :self.n_cats]),
              columns=domain.get_categorical_columns()
          )


          # Reconstruct ordinal features
          ord_synth = pd.DataFrame(
              values[:, self.n_cats : self.n_cats + self.n_ords].astype(np.int64),
              columns=domain.get_ordinal_columns()
          )

          # Reconstruct continous features
          con_synth = pd.DataFrame(
              values[:, self.n_cats + self.n_ords:],
              columns=domain.get_continuous_columns()
          )

          #Create synthetic df
          return pd.concat([cat_synth, ord_synth, con_synth], axis=1)
